#!/bin/bash
today=$(date +%F)
sec=$(date +%T)
echo ""
echo "### Composites Benchmark ###"
echo $today
echo $sec
uname -a
all_bench_start=$(date +%s)

for LIMIT in 50 100 500 1000 5000 10000
do
 #benchstart
 bench_start=$(date +%s)

 #zeroes
 himod=0
 ratio=0
 newratio=0

 #calculation
 for ((i=1; i<=$LIMIT; i++))
 do
  k=0
  mods=0
  if ! (($i % 2)); then
  for ((j=1; j<=i; j++))
  do
   if ! (($i % $j)); then
    mods=$(($mods + 1))
    k=$(($k+$j))
  fi
  done
   if(( $mods > 2 )); then
    summ=$(($k-$i))   
 #   if (($summ>$i)); then
 #   fi   

    if (($mods > $himod )); then
     himod=$(($mods))
     newratio=$(($i / $mods))
    fi
    if (($newratio > $ratio)); then
     ratio=$(($newratio))
     composite=$i
    fi
   fi
  fi
 done

 #output
 bench_end=$(date +%s)
 bench_time=$(($bench_end-$bench_start))
 #echo "Most composite number in $LIMIT is $composite, divisible by $himod numbers."
 echo "$LIMIT took $bench_time seconds to complete."
done
bench_time=$(($bench_end-$all_bench_start))
echo ""
echo "The test took $bench_time seconds to complete."